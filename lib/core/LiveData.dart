import 'dart:typed_data';

import 'package:flutter_controller/model/MonitorSettings.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
class DataWrapper {
  double valueDouble;
  int valueInt;
}
class ParameterWrapper
{
  int    paramNumber;
  double paramMinValue=0;
  double paramMaxValue=1;
  double paramValue = 1;
  String paramUnits;
  String paramFull;
}
class LiveData {
  final double udc; // Actual DC voltage (V)
  final double uz; // Motor voltage (V)
  final int iAmp; // Motor current (A)
  final int speed; // Motor speed (rad\s)
  final int pel; // Motor power electric (W)
  final int tFet; // controller plate temp (ºC)
  final double activeFlux; // Calculated flux of motor (Weber)
  final double brake; // Brake voltage
  final double tetaShift; // Hall phase shift (rad)
  final int tMot; // motor winding temp (ºC)
  final int hallCnt; // [unsigned] Sum of Hall inputs (0...6)
  final double throttleAct; // RAW voltage of Gas Lever (V)
  final int errorCode; //error code
  final int statusCode; //status code
  LiveData.fromBytes(Uint8List data)
      : udc = ByteData.view(data.buffer).getUint16(0, Endian.little) / 100.0,
        uz = ByteData.view(data.buffer).getUint16(2, Endian.little) / 100.0,
        iAmp = ByteData.view(data.buffer).getInt16(4, Endian.little),
        speed = ByteData.view(data.buffer).getInt16(6, Endian.little),
        pel = ByteData.view(data.buffer).getUint16(8, Endian.little),
        tFet = ByteData.view(data.buffer).getInt16(10, Endian.little),
        activeFlux =
            ByteData.view(data.buffer).getUint16(12, Endian.little) / 10000.0,
        brake =
            ByteData.view(data.buffer).getUint16(14, Endian.little) / 1000.0,
        tetaShift =
            ByteData.view(data.buffer).getInt16(16, Endian.little) / 1000.0,
        tMot = ByteData.view(data.buffer).getInt16(18, Endian.little),
        hallCnt = ByteData.view(data.buffer).getUint16(20, Endian.little),
        throttleAct =
            ByteData.view(data.buffer).getUint16(22, Endian.little) / 1000.0,
        statusCode = ByteData.view(data.buffer).getUint16(24, Endian.little),
        errorCode = ByteData.view(data.buffer).getUint16(26, Endian.little);

  @override
  String toString() =>
      "udc = $udc | uz = $uz | iAmp = $iAmp | speed = $speed | pel = $pel | tFet = $tFet | "
      "activeFlux = $activeFlux | brake = $brake | tetaShift = $tetaShift | tMot = $tMot | hallCnt = $hallCnt | throttleAct = $throttleAct ";

  /*function return string with parameter Units and sets additional data inside*/
  String getParameter(int paramNum, ParameterWrapper parameter) {
    switch (paramNum) {
      case 1:
        parameter.paramNumber = paramNum;
        parameter.paramValue = udc;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 110;
        parameter.paramUnits = " V";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " V";
      case 2:
        parameter.paramNumber = paramNum;
        parameter.paramValue = uz;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 110;
        parameter.paramUnits = " V";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " V";
      case 3:
        parameter.paramNumber = paramNum;
        parameter.paramValue = iAmp.toDouble();
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 600;
        parameter.paramUnits = " A";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " A";
      case 4:
        parameter.paramNumber = paramNum;
        parameter.paramValue = speed.toDouble();
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 6000;
        parameter.paramUnits = " Rad/s";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " Rad/s";
      case 5:
        parameter.paramNumber = paramNum;
        parameter.paramValue = (pel.toDouble()*0.001);
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 22;
        parameter.paramUnits = " kWt";
        parameter.paramFull = parameter.paramValue.toStringAsFixed(3)+parameter.paramUnits;
        return " kWt";
      case 6:
        parameter.paramNumber = paramNum;
        parameter.paramValue = tFet.toDouble();
        parameter.paramMinValue = -30;
        parameter.paramMaxValue = 110;
        parameter.paramUnits = " ºC";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " ºC";
      case 7:
        parameter.paramNumber = paramNum;
        parameter.paramValue = activeFlux;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 0.5;
        parameter.paramUnits = " Wb";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " Wb";
      case 8:
        parameter.paramNumber = paramNum;
        parameter.paramValue = brake;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 5;
        parameter.paramUnits = " V";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " V";
      case 9:
        parameter.paramNumber = paramNum;
        parameter.paramValue = tetaShift;
        parameter.paramMinValue = -1;
        parameter.paramMaxValue = 1;
        parameter.paramUnits = " Rad";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " Rad";
      case 10:
        parameter.paramNumber = paramNum;
        parameter.paramValue = tMot.toDouble();
        parameter.paramMinValue = -30;
        parameter.paramMaxValue = 180;
        parameter.paramUnits = " ºC";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " ºC";
      case 11:
        parameter.paramNumber = paramNum;
        parameter.paramValue = hallCnt.toDouble();
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 6;
        parameter.paramUnits = " ";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return "";
      case 12:
        parameter.paramNumber = paramNum;
        parameter.paramValue = throttleAct;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 5;
        parameter.paramUnits = " V";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " V";
      case 13:
        /*Convert string to Number type INT*/
        var wheelDiam = int.parse(Settings.getValue<String>(MonitorTabSettings.wheelDiameterMM, "660"));
        var motorPolePairs = int.tryParse(Settings.getValue(MonitorTabSettings.motorPolePairsKey, "1"));
        /*Convert string to Number type DOUBLE*/
        var mainGear = double.parse(Settings.getValue(MonitorTabSettings.mainGearKey, "1"));
        var secondGear = double.parse(Settings.getValue(MonitorTabSettings.secondGearKey, "1"));
        var diamMeter= (wheelDiam)/1000/60;
        parameter.paramNumber = paramNum;
        parameter.paramValue = (1/mainGear*1/secondGear*(1/motorPolePairs*9.55*speed)*diamMeter*3.6*3.141592).abs();
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 140;
        parameter.paramUnits = " km/h";
        parameter.paramFull = parameter.paramValue.toStringAsFixed(0)+parameter.paramUnits;
        return " V";
      case 14:
        parameter.paramNumber = paramNum;
        parameter.paramValue = 0;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 1;
        parameter.paramUnits = " ";
        parameter.paramFull = parameter.paramValue.toString()+parameter.paramUnits;
        return " V";

      default:
        parameter.paramNumber = paramNum;
        parameter.paramValue = 0;
        parameter.paramMinValue = 0;
        parameter.paramMaxValue = 1;
        parameter.paramUnits = " 0";
        parameter.paramFull = "Unknown";
        return "";
    }
  }

}
