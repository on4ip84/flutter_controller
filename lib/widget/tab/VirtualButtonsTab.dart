import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_controller/bloc/VirtualButtonsBloc.dart';
import 'package:flutter_controller/di/Provider.dart';
import 'package:flutter_controller/model/VirtualButtonsSettings.dart';
import 'package:flutter_controller/model/Parameter.dart';
import 'package:flutter_controller/widget/tab/CommonSettingsTab.dart';

class VirtualButtonsTab extends StatefulWidget {
  @override
  _VirtualButtonsTabState createState() => _VirtualButtonsTabState();
}

/*
  Создает класс виртуальных кнопок наследую класс общих настроек  в контструктор
  класса общих настроек передается видгет экрана и тип данных для контроллера
   потока данных
 */
class _VirtualButtonsTabState
    extends CommonSettingsTabState<VirtualButtonsTab, VirtualButtonsSettings> {
  static const _virtualButtonsparameterNames = "virtualButtonsParameterNames";
  static const _possibleNegativeValues = [];
  static const _onlyIntegerValues = [];
  final _formKey = GlobalKey<FormState>();

  VirtualButtonsTabBloc _buttonsTabBloc;
  Map _parameterNames;

  Map<String, String Function(String, String)> _validators;
  static const _saveTitle = "saveTitle";
  static const _saveMessage = "saveMessage";
  StreamSubscription<VirtualButtonsSettings> _subscription;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _buttonsTabBloc = Provider.of(context).buttonsTabBloc;
    _parameterNames = localizedStrings[_virtualButtonsparameterNames];

    if (_subscription == null) {
      _subscription = _buttonsTabBloc.buttonsViewModelStream.listen((event) {
        writeOkDialog(_buttonsTabBloc.writeResponseState);
      });
    }

    _validators = {
      "default": (String value, String variableName) {
        double number = double.tryParse(value);
        if (number == null) {
          return localizedStrings[CommonSettingsTabState.NOT_NUMBER];
        }
        if (_onlyIntegerValues.contains(variableName) &&
            number.round() != number) {
          return localizedStrings[CommonSettingsTabState.NOT_INTEGER_NUMBER];
        }
        if (!_possibleNegativeValues.contains(variableName) && number < 0) {
          return localizedStrings[CommonSettingsTabState.LESS_THAN_ZERO];
        }

        return null;
      },
    };

    /*
      При обновлении экрана в поток добавляется команда для считывания данных
      Слушатель потока на нее отриагирует.
     */
    _buttonsTabBloc.buttonsSettingsCommandStream
        .add(ButtonsSettingsCommand.READ);
  }

  @override
  Widget buildRow(String parameterName, String value, String variableName) {
    String Function(String) validator = (String value) {
      String validationResult = validateNotNullOrEmpty(value, variableName);

      if (validationResult == null) {
        validationResult = (_validators[variableName] ?? _validators["default"])
            ?.call(value, variableName);
      }

      return validationResult;
    };

    Widget buttonField = _buildButton(variableName, value);
    if (variableName == 'input1Real' ||
        variableName == 'input2Real' ||
        variableName == 'input3Real' ||
        variableName == 'input4Real')
      return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Expanded(
          flex: 4,
          child: Container(
            height: 40,
            child: Align(
              alignment: Alignment(-1, 0),
              child: Text(
                parameterName,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
        ),
        Text(value, style: TextStyle(fontSize: 20)),
        Container(width: 20,)
      ]);
    else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 4,
          ),
          Expanded(
            flex: 4,
            child: Container(
              height: 40,
              child: Align(
                alignment: Alignment(-1, 0),
                child: Text(
                  parameterName,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              height: 100,
              //child: Align(alignment: Alignment(0.5, 0), child: inputField),
              child: Align(alignment: Alignment(0.5, 0), child: buttonField),
            ),
          ),
          Container(
            width: 10,
            height: 80,
          ),
        ],
      );
    }
  }

  writeOkDialog(int state) {
    if (state == 1) {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text(localizedStrings[_saveTitle]),
                content: Text(localizedStrings[_saveMessage]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => {
                      _buttonsTabBloc.writeResponseState = 0,
                      Navigator.pop(context, 'OK'),
                    },
                    child: const Text('OK'),
                  ),
                ],
              ));
    }
  }

  /*
   сконструируем кноку для каждого входа
   */
  Container _buildButton(String variableName, String value) {
    return Container(
      height: 50,
      width: 100,
      child: ElevatedButton(
        onPressed: () {
          changeState(variableName, value);
        },
        child: Text(value == "0" ? "OFF" : "ON"),
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.green,
          foregroundColor: Colors.white,
          shadowColor: Colors.greenAccent,
          elevation: 15,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(32.0)),
          minimumSize: Size(340, 70), //////// HERE
        ),
      ),
    );
  }

  changeState(String variableName, String value) {
    var valInt = int.tryParse(value);
    String newVlue = (valInt > 0) ? "0" : "1";
    _buttonsTabBloc.buttonsSettingsDataStream
        .add(Parameter(variableName, newVlue));
    _buttonsTabBloc.buttonsSettingsCommandStream
        .add(ButtonsSettingsCommand.REFRESH);
  }
  @override
  void dispose() {
    _subscription.cancel();
    // ignore: avoid_print
    print('Dispose used');
    super.dispose();
  }
  @override
  GlobalKey<State<StatefulWidget>> getFormKey() => _formKey;

  @override
  Map getParameterNames() => _parameterNames;

  @override
  Map getParameterValues(AsyncSnapshot<VirtualButtonsSettings> snapshot) {
    if (snapshot.hasData) {
      return snapshot.data.toJson();
    } else {
      return Map<String, dynamic>();
    }
  }

  @override
  Stream<VirtualButtonsSettings> getViewModelStream() =>
      _buttonsTabBloc.buttonsViewModelStream;

  @override
  void onRead() {
    _buttonsTabBloc.buttonsSettingsCommandStream
        .add(ButtonsSettingsCommand.READ);
  }

  @override
  void onSave() {
    _buttonsTabBloc.buttonsSettingsCommandStream
        .add(ButtonsSettingsCommand.SAVE);
  }

  @override
  void onWrite() {
    if (!_formKey.currentState.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              localizedStrings[CommonSettingsTabState.WRITING_NOT_ALLOWED])));
      return;
    } else {
      _formKey.currentState.save();
      _buttonsTabBloc.buttonsSettingsCommandStream
          .add(ButtonsSettingsCommand.WRITE);
    }
  }

  static const _read = "read";
  static const _write = "write";

  @override
  Widget buildBottomButtons() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          ElevatedButton(
            child: Text(localizedStrings[_read]),
            onPressed: () {
              onRead();
            },
          ),
          Expanded(
            child: Container(),
          ),
          ElevatedButton(
            onPressed: () {
              onWrite();
            },
            child: Text(localizedStrings[_write]),
          ),
        ],
      ),
    );
  }
}
