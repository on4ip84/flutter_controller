import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_controller/bloc/RegTabBloc.dart';
import 'package:flutter_controller/di/Provider.dart';
import 'package:flutter_controller/model/Parameter.dart';
import 'package:flutter_controller/model/RegSettings.dart';
import 'package:flutter_controller/widget/tab/CommonSettingsTab.dart';

class RegTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegTabState();

}

class _RegTabState extends CommonSettingsTabState<RegTab, RegSettings> {
  static const _possibleNegativeValues = [];
  static const _onlyIntegerValues = [];
  static const _notLessOne=["mainGear","secondGear"];
  static const _regParameterNames = "regParameterNames";

  final _formKey = GlobalKey<FormState>();

  RegTabBloc _regTabBloc;
  Map _parameterNames;
  Map<String, String Function(String, String)> _validators;
  static const _saveTitle = "saveTitle";
  static const _saveMessage = "saveMessage";
  StreamSubscription<RegSettings> _subscription;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _regTabBloc = Provider.of(context).regTabBloc;
    _parameterNames = localizedStrings[_regParameterNames];

    if (_subscription == null) {
      _subscription = _regTabBloc.regViewModelStream.listen((event) {
        writeOkDialog(_regTabBloc.writeResponseState);
      });
    }
    _validators = {
      "default": (String value, String variableName) {
        double number = double.tryParse(value);
        if (number == null) {
          return localizedStrings[CommonSettingsTabState.NOT_NUMBER];
        }
        if (_onlyIntegerValues.contains(variableName) && number.round() != number) {
          return localizedStrings[CommonSettingsTabState.NOT_INTEGER_NUMBER];
        }
        if (_notLessOne.contains(variableName) && number== 0) {
          return localizedStrings[CommonSettingsTabState.WRITING_NOT_ALLOWED];
        }
        if (!_possibleNegativeValues.contains(variableName) && number < 0) {
          return localizedStrings[CommonSettingsTabState.LESS_THAN_ZERO];
        }

        return null;
      },
    };

    _regTabBloc.regSettingsCommandStream.add(RegSettingsCommand.READ);
  }


  @override
  Widget buildRow(String parameterName, String value, String variableName) {
    String Function(String) validator = (String value) {
      String validationResult = validateNotNullOrEmpty(value, variableName);

      if (validationResult == null) {
        validationResult = (_validators[variableName] ?? _validators["default"])?.call(value, variableName);
      }

      return validationResult;
    };

    Widget inputField = _buildTextInput(validator, variableName, value);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 4,
        ),
        Expanded(
          flex: 4,
          child: Container(
            height: 40,
            child: Align(
              alignment: Alignment(-1, 0),
              child: Text(parameterName),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            height: 40,
            child: Align(alignment: Alignment(0.5, 0), child: inputField),
          ),
        ),
        Container(
          width: 4,
        )
      ],
    );
  }

  TextFormField _buildTextInput(validator, String variableName, String value) {
    return TextFormField(
      validator: validator,
      onSaved: (value) {
        _regTabBloc.regSettingsDataStream.add(Parameter(variableName, value));
      },
      controller: TextEditingController()..text = value,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        errorStyle: TextStyle(height: 0), // Use just red border without text
        border: OutlineInputBorder()));
  }

  writeOkDialog(int state) {
    if (state == 1) {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: Text(localizedStrings[_saveTitle]),
            content: Text(localizedStrings[_saveMessage]),
            actions: <Widget>[
              TextButton(
                onPressed: () => {
                  _regTabBloc.writeResponseState = 0,
                  Navigator.pop(context, 'OK'),
                },
                child: const Text('OK'),
              ),
            ],
          ));
    }
    if(state == 2){
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: Text(localizedStrings[_saveTitle]),
            content: Text("Saved to ROM"),
            actions: <Widget>[
              TextButton(
                onPressed: () => {
                  _regTabBloc.writeResponseState = 0,
                  Navigator.pop(context, 'OK'),
                },
                child: const Text('OK'),
              ),
            ],
          ));
    }
  }
  @override
  void dispose() {
    _subscription.cancel();
    // ignore: avoid_print
    print('Dispose used');
    super.dispose();
  }
  @override
  Map getParameterNames() => _parameterNames;

  @override
  Map getParameterValues(AsyncSnapshot<RegSettings> snapshot) {
    if (snapshot.hasData) {
      return snapshot.data.toJson();
    } else {
      return Map<String, dynamic>();
    }
  }

  @override
  Stream<RegSettings> getViewModelStream() => _regTabBloc.regViewModelStream;

  @override
  void onRead() {
    _regTabBloc.regSettingsCommandStream.add(RegSettingsCommand.READ);
  }

  @override
  void onSave() {
    _regTabBloc.regSettingsCommandStream.add(RegSettingsCommand.SAVE);
  }

  @override
  void onWrite() {
    if (!_formKey.currentState.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(localizedStrings[CommonSettingsTabState.WRITING_NOT_ALLOWED])));
      return;
    } else {
      _formKey.currentState.save();
      _regTabBloc.regSettingsCommandStream.add(RegSettingsCommand.WRITE);
    }
  }

  @override
  GlobalKey<State<StatefulWidget>> getFormKey() => _formKey;
}
