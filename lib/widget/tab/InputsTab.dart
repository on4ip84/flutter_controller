import 'package:flutter/material.dart';
import 'package:flutter_controller/di/Provider.dart';
import 'package:flutter_controller/bloc/InputsTabBloc.dart';
import 'package:flutter_controller/model/InputsSettings.dart';
import 'package:flutter_screen_wake/flutter_screen_wake.dart';
import 'package:flutter_controller/widget/TitledCard.dart';

class InputsTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _InputsTabState();
}

class _InputsTabState extends State<InputsTab> {
  InputsTabBloc _inputsTabBloc;
  final _formKey = GlobalKey<FormState>();
  static const _ANALOG = "Analog Inputs";
  static const _INPUTS = "Controller Inputs";
  static const _CANINPUTS = "Virtual Inputs";
  static const double _ROWHEIGHT = 30;
  @override
  void dispose() {
    _inputsTabBloc.monitorSettingsCommandStream
        .add(InputsTabCommand.STOP_MONITORING);
    super.dispose();
    FlutterScreenWake.keepOn(false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    FlutterScreenWake.keepOn(true);

    _inputsTabBloc = Provider.of(context).inputsTabBloc;
    _inputsTabBloc.monitorSettingsCommandStream.add(InputsTabCommand.READ);
  }

  @override
  Widget build(BuildContext context) {
    _inputsTabBloc.monitorSettingsCommandStream
        .add(InputsTabCommand.START_MONITORING);
    return StreamBuilder<InputsSettings>(
        stream: _inputsTabBloc.monitorSettingsStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
            child: _buildContent(context, snapshot.data),
          );
        });
  }

  Widget _buildContent(BuildContext context, InputsSettings _settings) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _buildAnalog(context, _settings),
                  _buildInputs(context, _settings),
                  _buildVirtualInputs(context, _settings)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAnalog(BuildContext context, InputsSettings settings) {
    final s1MaxTorqueCurrent = "s1MaxTorqueCurrent";

    return TitledCard(
        title: _ANALOG,
        child: Column(
          children: [
            _buildRow(
                "Vdc_ADC", settings.vdcAdc.toString(), s1MaxTorqueCurrent),
            _buildRow("Ia_ADC", settings.iaAdc.toString(), s1MaxTorqueCurrent),
            _buildRow("Ic_ADC", settings.icAdc.toString(), s1MaxTorqueCurrent),
            _buildRow("Encoder Counts", settings.encoderCounts.toString(),
                s1MaxTorqueCurrent),
            _buildRow("PWM Counts", settings.pwnCounts.toString(),
                s1MaxTorqueCurrent),
            _buildRow(
                "HALL Counts", settings.hallCnt.toString(), s1MaxTorqueCurrent),
            _buildRow("Throttle Voltage", settings.throttleVolts.toString(),
                s1MaxTorqueCurrent),
            _buildRow("Brake Voltage", settings.brakeVolts.toString(),
                s1MaxTorqueCurrent),
            _buildRow("System Supply", settings.v12volts.toString(),
                s1MaxTorqueCurrent),
            Container(
              height: 8,
            )
          ],
        ));
  }

  Widget _buildInputs(BuildContext context, InputsSettings settings) {
    final s1MaxTorqueCurrent = "s1MaxTorqueCurrent";

    return TitledCard(
        title: _INPUTS,
        child: Column(
          children: [
            _buildRow(
                "IN 1 input", settings.in1.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "IN 2 input", settings.in2.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "IN 3 input", settings.in3.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "IN 4 input", settings.in4.toString(), s1MaxTorqueCurrent),
            Container(
              height: 15,
            )
          ],
        ));
  }

  Widget _buildVirtualInputs(BuildContext context, InputsSettings settings) {
    final s1MaxTorqueCurrent = "s1MaxTorqueCurrent";

    return TitledCard(
        title: _CANINPUTS,
        child: Column(
          children: [
            _buildRow(
                "CAN 1 input", settings.in1Can.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "CAN 2 input", settings.in2Can.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "CAN 3 input", settings.in3Can.toString(), s1MaxTorqueCurrent),
            _buildRow(
                "CAN 4 input", settings.in4Can.toString(), s1MaxTorqueCurrent),
            Container(
              height: 15,
            )
          ],
        ));
  }

  Widget _buildRow(String parameterName, String value, String variableName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 4,
        ),
        Expanded(
          flex: 4,
          child: Container(
            height: _ROWHEIGHT,
            child: Align(
              alignment: Alignment(-1, 0),
              child: Text(parameterName),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            height: _ROWHEIGHT,
            child: Align(alignment: Alignment(0.5, 0), child: Text(value)),
          ),
        ),
        Container(
          width: 4,
        )
      ],
    );
  }
}
