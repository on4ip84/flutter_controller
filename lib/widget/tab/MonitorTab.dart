import 'package:flutter/material.dart';
import 'package:flutter_controller/bloc/MonitorTabBloc.dart';
import 'package:flutter_controller/di/Provider.dart';
import 'package:flutter_controller/model/MonitorSettings.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_screen_wake/flutter_screen_wake.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class MonitorTab extends StatefulWidget {
  @override
  _MonitorTab createState() => new _MonitorTab();
}

class _MonitorTab extends State<MonitorTab> {
  MonitorTabBloc _monitorTabBloc;
  Map _localizedStrings;

  // Здесь задается ключ к поиску названий паметров из списка, параметры выводятся на главный экран
  static const _monitorSettingsParameters = "monitorSettingsParameters";
  Map<int, String> _paramNames;

  @override
  void dispose() {
    _monitorTabBloc.monitorSettingsCommandStream
        .add(MonitorTabCommand.STOP_MONITORING);
    FlutterScreenWake.keepOn(false);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    FlutterScreenWake.keepOn(true);

    _localizedStrings = Provider.of(context).localizedStrings;
    _monitorTabBloc = Provider.of(context).monitorTabBloc;
    _monitorTabBloc.monitorSettingsCommandStream.add(MonitorTabCommand.READ);
    _paramNames = (_localizedStrings[_monitorSettingsParameters] as Map)
        .map((key, value) => MapEntry(int.parse(key), value));
  }

  /*My methods*/
  Color getColorError(String error) {
    if (error == "NO") {
      return Colors.green;
    }
    return Colors.redAccent;
  }

  void _handleLogStart() {
    //_monitorTabBloc.logOnOff();
  }

  @override
  Widget build(BuildContext context) {
    _monitorTabBloc.monitorSettingsCommandStream
        .add(MonitorTabCommand.START_MONITORING);

    //final large = GoogleFonts.oswald(textStyle: TextStyle(fontSize: 80));
    final small = GoogleFonts.oswald(textStyle: TextStyle(fontSize: 20));
    /*Check state of logging switch and make sense*/
    //_monitorTabBloc.logOnOff();
    return StreamBuilder<MonitorTabSettingsData>(
        stream: _monitorTabBloc.monitorSettingsStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          return Container(
            padding: EdgeInsets.all(8),
            child: Column(
                /*main screen alignment property*/
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      width: 350,
                      height: 330,
                      child: FittedBox(
                          fit: BoxFit.fitHeight,
                          //   child: Text(snapshot.data.centerParam,
                          //        textAlign: TextAlign.center, style: large)),
                          child: Column(children: <Widget>[
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Text(_paramNames[snapshot
                                      .data.centerParameter.paramNumber]),
                                  Container(
                                    width: 120,
                                  ),
                                  Text("Logging"),
                                  Container(
                                    width: 40,
                                    height: 40,
                                    child: IconButton(
                                      icon: Icon(Icons.save_alt,
                                          color:
                                              _monitorTabBloc.getLogState() > 0
                                                  ? Colors.green
                                                  : Colors.black),
                                      //onPressed: _handleLogStart,
                                        onPressed: (){},
                                    ),
                                  )
                                ]),
                            SfRadialGauge(axes: <RadialAxis>[
                              RadialAxis(
                                  minimum: snapshot
                                      .data.centerParameter.paramMinValue,
                                  maximum: snapshot
                                      .data.centerParameter.paramMaxValue,
                                  ranges: <GaugeRange>[
                                    GaugeRange(
                                        startValue: snapshot
                                            .data.centerParameter.paramMinValue,
                                        endValue: snapshot
                                            .data.centerParameter.paramMaxValue,
                                        startWidth: 0.1,
                                        sizeUnit: GaugeSizeUnit.factor,
                                        endWidth: 0.1,
                                        gradient: SweepGradient(stops: <double>[
                                          0.2,
                                          0.5,
                                          0.75
                                        ], colors: <Color>[
                                          Colors.green,
                                          Colors.yellow,
                                          Colors.red
                                        ]))
                                  ],
                                  pointers: <GaugePointer>[
                                    NeedlePointer(
                                        value: snapshot
                                            .data.centerParameter.paramValue)
                                  ],
                                  annotations: <GaugeAnnotation>[
                                    GaugeAnnotation(
                                        widget: Container(
                                            child: Text(
                                                snapshot.data.centerParameter
                                                    .paramFull,
                                                style: TextStyle(
                                                    fontSize: 25,
                                                    fontWeight:
                                                        FontWeight.bold))),
                                        angle: 90,
                                        positionFactor: 0.5)
                                  ])
                            ]),
                          ]))),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                            child: Column(children: <Widget>[
                          FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                  _paramNames[
                                      snapshot.data.leftTop.paramNumber],
                                  textAlign: TextAlign.center)),
                          Text(snapshot.data.leftTop.paramFull, style: small),
                          Divider(),
                          FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                  _paramNames[
                                      snapshot.data.leftBottom.paramNumber],
                                  textAlign: TextAlign.center)),
                          Text(snapshot.data.leftBottom.paramFull,
                              style: small),
                        ])),
                        Expanded(
                            child: Column(children: <Widget>[
                          FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                  _paramNames[
                                      snapshot.data.rightTop.paramNumber],
                                  textAlign: TextAlign.center)),
                          Text(snapshot.data.rightTop.paramFull,
                              textAlign: TextAlign.center, style: small),
                          Divider(),
                          FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Text(
                                  _paramNames[
                                      snapshot.data.rightBottom.paramNumber],
                                  textAlign: TextAlign.center)),
                          Text(snapshot.data.rightBottom.paramFull,
                              style: small),
                        ])),
                      ]),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 170,
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            padding: EdgeInsets.all(4),
                            child: Column(children: <Widget>[
                              Text(_localizedStrings["error"], style: small),
                              Divider(),
                              InkWell(
                                onTap: snapshot.data.colorCodesState[3] ==
                                        Colors.green
                                    ? () {
                                        errorCodeShow(snapshot);
                                      }
                                    : null,
                                child: snapshot.data.colorCodesState[3] ==
                                        Colors.green
                                    ? Icon(
                                        Icons.error,
                                        size: 64,
                                        color: Colors.red,
                                      )
                                    : Icon(
                                        Icons.error_outline,
                                        size: 64,
                                        color: Colors.green,
                                      ),
                              )
                            ])),
                      ),
                      Container(
                          width: 100,
                          height: 170,
                          padding: EdgeInsets.all(4),
                          //color: Colors.green,
                          child: Column(children: <Widget>[
                            Text(snapshot.data.mode,
                                style: TextStyle(
                                    color: snapshot.data.colorCodesState[2] ==
                                            Colors.green
                                        ? Colors.green
                                        : Colors.black,
                                    fontSize: 30)),
                            Divider(),
                            snapshot.data.colorCodesState[1] == Colors.grey
                                ? ImageIcon(AssetImage("assets/images/parking.png"),color: Colors.red,size: 70,)
                                : snapshot.data.colorCodesState[8] ==
                                        Colors.green
                                    ? ImageIcon(AssetImage("assets/images/brake.png"),color: Colors.green,size: 70,)
                                    : snapshot.data.colorCodesState[6] ==
                                            Colors.green
                                        ? ImageIcon(AssetImage("assets/images/cruise.png"),color: Colors.green,size: 70,)
                                        : Container(
                                            width: 64,
                                            height: 64,
                                          ),
                          ])),
                      Container(
                          width: 100,
                          height: 170,
                          padding: EdgeInsets.all(8),
                          //color: Colors.green,
                          child: Column(children: <Widget>[
                            FittedBox(
                                fit: BoxFit.contain,
                                child: Text(_localizedStrings["profile"],
                                    style: small)),
                            Divider(),
                            InkWell(
                                onTap: () {
                                  print("${snapshot.data.mode}  object");
                                  stateCodeShow(snapshot);
                                },
                                child: Text(snapshot.data.profile,
                                    style: TextStyle(
                                        color: Colors.green, fontSize: 50)))
                          ]))
                    ],
                  ),
                ]),
          );
        });
  }

  void errorCodeShow(AsyncSnapshot<MonitorTabSettingsData> snapshot) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(_localizedStrings["error"]),
          content: ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: snapshot.data.entriesError.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 20,
                color: snapshot.data.colorCodesError[index],
                child:
                    Center(child: Text('${snapshot.data.entriesError[index]}')),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ),
          actions: [
            TextButton(
              onPressed: () {
                // GetBack
                Navigator.pop(context);
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }

  void stateCodeShow(AsyncSnapshot<MonitorTabSettingsData> snapshot) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(_localizedStrings["state"]),
          content: ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: snapshot.data.entriesState.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 20,
                color: snapshot.data.colorCodesState[index],
                child:
                    Center(child: Text('${snapshot.data.entriesState[index]}')),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ),
          actions: [
            TextButton(
              onPressed: () {
                // GetBack
                Navigator.pop(context);
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
