import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_controller/core/Packet.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/DriveSettings.dart';
import 'package:flutter_controller/model/Parameter.dart';
import 'package:flutter_controller/util/Mapper.dart';

enum DriveSettingsCommand { READ, WRITE, SAVE, REFRESH }

enum DriveControlMode { DISABLED, FOC, SENSORLESS, BLDC }

enum DrivePwmFrequency {
  PWM8000,
  PWM10000,
  PWM12000,
  PWM15000,
  PWM18000,
  PWM20000,
  PWM22000
}

class DriveTabBloc {
  static const SCREEN_NUMBER = 2;
  var writeResponseState = 0;
  BluetoothInteractor _bluetoothInteractor;
  DriveSettings _driveSettings;

  StreamController _driveViewModelStreamController =
      StreamController<DriveSettings>.broadcast();

  Stream get driveViewModelStream => _driveViewModelStreamController.stream;

  StreamController<DriveSettingsCommand> _driveSettingsCommandStreamController =
      StreamController<DriveSettingsCommand>.broadcast();

  StreamSink<DriveSettingsCommand> get driveSettingsCommandStream =>
      _driveSettingsCommandStreamController.sink;

  StreamController<
      Parameter> _driveSettingsDataStreamController = StreamController<
          Parameter>.broadcast(
      sync:
          true); //Sync to avoid async between changing parameters and writing to controller
  StreamSink<Parameter> get driveSettingsDataStream =>
      _driveSettingsDataStreamController.sink;

  DriveTabBloc(this._bluetoothInteractor) {
    _driveSettingsCommandStreamController.stream.listen(_handleCommand);
    _driveSettingsDataStreamController.stream.listen(_handleSettingsData);
  }

  void _handleSettingsData(Parameter parameter) {
    switch (parameter.name) {
      case "throttleUpSpeed":
        _driveSettings.throttleUpSpeed = int.parse(parameter.value);
        break;
      case "throttleDownSpeed":
        _driveSettings.throttleDownSpeed = int.parse(parameter.value);
        break;
      case "throttlePhaseCurrentMax":
        _driveSettings.throttlePhaseCurrentMax = int.parse(parameter.value);
        break;

      case "brakeUpSpeed":
        _driveSettings.brakeUpSpeed = int.parse(parameter.value);
        break;
      case "brakeDownSpeed":
        _driveSettings.brakeDownSpeed = int.parse(parameter.value);
        break;
      case "brakePhaseCurrentMax":
        _driveSettings.brakePhaseCurrentMax = int.parse(parameter.value);
        break;

      case "discreetBrakeCurrentUpSpeed":
        _driveSettings.discreetBrakeCurrentUpSpeed = int.parse(parameter.value);
        break;
      case "discreetBrakeCurrentDownSpeed":
        _driveSettings.discreetBrakeCurrentDownSpeed =
            int.parse(parameter.value);
        break;
      case "discreetBrakeCurrentMax":
        _driveSettings.discreetBrakeCurrentMax = int.parse(parameter.value);
        break;
      case "autoBrakeCurrent":
        _driveSettings.autoBrakeMaxCurrent = int.parse(parameter.value);
        break;
      case "pwmFrequency":
        print("motorParameter.value = ${parameter.value}");
        final index = parameter.value.trim().indexOf(".PWM") + ".PWM".length;
        final frequency = parameter.value.trim().substring(index);
        _driveSettings.pwmFrequency = int.parse(frequency);
        break;
      case "controlMode":
        var sensorType = DriveControlMode.values
            .firstWhere((element) => element.toString() == parameter.value);
        _driveSettings.controlMode =
            DriveControlMode.values.indexOf(sensorType);
        break;
      case "fwMode":
        var fwMask = 1 << 3;
        if (parameter.value == "true") {
          _driveSettings.fwMode = 1;
          _driveSettings.controlMode |= fwMask;
        } else {
          _driveSettings.fwMode = 0;
          _driveSettings.controlMode &= (~fwMask);
        }
        break;
      case "autoBrake":
        var fwMask = 1 << 4;
        if (parameter.value == "true") {
          _driveSettings.autoBrake = 1;
          _driveSettings.controlMode |= fwMask;
        } else {
          _driveSettings.autoBrake = 0;
          _driveSettings.controlMode &= (~fwMask);
        }
        break;
      case "decouple":
        var fwMask = 1 << 5;
        if (parameter.value == "true") {
          _driveSettings.decouple = 1;
          _driveSettings.controlMode |= fwMask;
        } else {
          _driveSettings.decouple = 0;
          _driveSettings.controlMode &= (~fwMask);
        }
        break;
      case "dpwm":
        var fwMask = 1 << 6;
        if (parameter.value == "true") {
          _driveSettings.dpwm = 1;
          _driveSettings.controlMode |= fwMask;
        } else {
          _driveSettings.dpwm = 0;
          _driveSettings.controlMode &= (~fwMask);
        }
        break;
    }
  }

  void _handleCommand(DriveSettingsCommand event) {
    switch (event) {
      case DriveSettingsCommand.READ:
        _driveSettingsRead();
        break;
      case DriveSettingsCommand.WRITE:
        _driveSettingsWrite();
        break;
      case DriveSettingsCommand.SAVE:
        _driveSettingsSave();
        break;
      case DriveSettingsCommand.REFRESH:
        _driveSettingsRefresh();
        break;
    }
  }

  void _driveSettingsRead() {
    _bluetoothInteractor.sendMessage(Packet(SCREEN_NUMBER, 0, Uint8List(28)));
    _bluetoothInteractor.startListenSerial(_packetHandler);
  }

  void _driveSettingsRefresh() {
    _driveViewModelStreamController.sink.add(_driveSettings);
  }

  void _driveSettingsWrite() {
    Packet packet = Mapper.driveSettingsToPacket(_driveSettings);
    _bluetoothInteractor.sendMessage(packet);
  }

  void _driveSettingsSave() {
    _bluetoothInteractor.save();
  }

  void _packetHandler(Packet packet) {
    if (packet.screenNum == SCREEN_NUMBER) {
      if(packet.cmd == readCMD)
      {
        _driveSettings = Mapper.packetToDriveSettings(packet);
        _driveSettingsRefresh();
      }
      if(packet.cmd == responseOK)
      {
        writeResponseState = 1;
        _driveSettingsRefresh();
      }
    }
    if(packet.screenNum == 127) //Save response
        {
      writeResponseState = 2;
      _driveSettingsRefresh();
    }
  }

  void dispose() {
    _bluetoothInteractor.stopListenSerial();
    _driveViewModelStreamController.close();
    _driveSettingsCommandStreamController.close();
    _driveSettingsDataStreamController.close();
  }
}
