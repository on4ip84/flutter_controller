import 'dart:async';
import 'dart:typed_data';
import 'package:flutter_controller/core/Packet.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/InputsSettings.dart';

enum InputsTabCommand { READ, START_MONITORING, STOP_MONITORING }

class InputsTabBloc {
  /*Inputs screen number*/
  static const _SCREEN_NUMBER = 6;

  /*monitoring time in ms*/
  static const _MONITOR_TIME = 200;

  /*make classes */
  BluetoothInteractor _bluetoothInteractor;

  /*Make streams*/
  StreamSubscription _monitoringSubscription;

  StreamController<InputsTabCommand> _monitorSettingsCommandStreamController =
      StreamController<InputsTabCommand>.broadcast();

  StreamSink<InputsTabCommand> get monitorSettingsCommandStream =>
      _monitorSettingsCommandStreamController.sink;

  StreamController<InputsSettings> _monitorSettingsStreamController =
      StreamController<InputsSettings>.broadcast();

  Stream<InputsSettings> get monitorSettingsStream =>
      _monitorSettingsStreamController.stream;

  /*Make constructor for class*/
  InputsTabBloc(this._bluetoothInteractor) {
    _monitorSettingsCommandStreamController.stream.listen(_handleCommand);
  }

  _startMonitoring() {
    _bluetoothInteractor.startListenSerial(_packetHandler);
    dynamic Function(int) computation = (count) {
      _bluetoothInteractor
          .sendMessage(Packet(_SCREEN_NUMBER, 0, Uint8List(28)));
    };
    if (_monitoringSubscription == null) {
      _monitoringSubscription =
          Stream.periodic(Duration(milliseconds: _MONITOR_TIME), computation)
              .listen((_) {});
    }
  }

  void _stopMonitoring() {
    _bluetoothInteractor.stopListenSerial();
    _monitoringSubscription.cancel();
    _monitoringSubscription = null;
  }

  void _packetHandler(Packet packet) {
    if (packet.screenNum == _SCREEN_NUMBER) {
      //LiveData liveDataStub = LiveData.fromBytes(packet.dataBuffer);

      InputsSettings _settings = InputsSettings.zero();
      _settings.vdcAdc = ByteData.view(packet.dataBuffer.buffer).getUint16(0, Endian.little);
      _settings.iaAdc = ByteData.view(packet.dataBuffer.buffer).getUint16(2, Endian.little);
      _settings.icAdc = ByteData.view(packet.dataBuffer.buffer).getUint16(4, Endian.little);
      _settings.encoderCounts= ByteData.view(packet.dataBuffer.buffer).getUint16(6, Endian.little);
      _settings.pwnCounts = ByteData.view(packet.dataBuffer.buffer).getUint16(8, Endian.little);
      _settings.in1 = ByteData.view(packet.dataBuffer.buffer).getUint16(10, Endian.little);
      _settings.in2 = ByteData.view(packet.dataBuffer.buffer).getUint16(12, Endian.little);
      _settings.in3 = ByteData.view(packet.dataBuffer.buffer).getUint16(14, Endian.little);
      _settings.in4 = ByteData.view(packet.dataBuffer.buffer).getUint16(16, Endian.little);
      _settings.in1Can = (ByteData.view(packet.dataBuffer.buffer).getUint16(18, Endian.little)>>0) & 0x01;
      _settings.in2Can = (ByteData.view(packet.dataBuffer.buffer).getUint16(18, Endian.little)>>1) & 0x01;
      _settings.in3Can = (ByteData.view(packet.dataBuffer.buffer).getUint16(18, Endian.little)>>2) & 0x01;
      _settings.in4Can = (ByteData.view(packet.dataBuffer.buffer).getUint16(18, Endian.little)>>3) & 0x01;
      _settings.hallCnt = ByteData.view(packet.dataBuffer.buffer).getUint16(20, Endian.little);
      _settings.throttleVolts = ByteData.view(packet.dataBuffer.buffer).getUint16(22, Endian.little) / 1000.0;
      _settings.brakeVolts = ByteData.view(packet.dataBuffer.buffer).getUint16(24, Endian.little) / 1000.0;
      _settings.v12volts = ByteData.view(packet.dataBuffer.buffer).getUint16(26, Endian.little) / 100.0;
      _monitorSettingsStreamController.sink.add(_settings);
    }
  }

  void _handleCommand(InputsTabCommand command) {
    switch (command) {
      case InputsTabCommand.READ:
        {
          _startMonitoring();
        }
        break;
      case InputsTabCommand.START_MONITORING:
        _startMonitoring();
        break;
      case InputsTabCommand.STOP_MONITORING:
        _stopMonitoring();
        break;
    }
  }

  void dispose() {
    _stopMonitoring();
    _monitorSettingsStreamController.close();
    _monitorSettingsCommandStreamController.close();
  }
}
