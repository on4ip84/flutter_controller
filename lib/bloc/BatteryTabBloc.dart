import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_controller/core/Packet.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/BatterySettings.dart';
import 'package:flutter_controller/model/Parameter.dart';
import 'package:flutter_controller/util/Mapper.dart';

enum ButtonsSettingsCommand { READ, WRITE, SAVE, REFRESH }

class BatteryTabBloc {
  static const SCREEN_NUMBER = 4;
  var writeResponseState = 0;
  BluetoothInteractor _bluetoothInteractor;
  BatterySettings _batterySettings;

  StreamController _batteryViewModelStreamController = StreamController<BatterySettings>.broadcast();

  Stream get batteryViewModelStream => _batteryViewModelStreamController.stream;

  StreamController<ButtonsSettingsCommand> _batterySettingsCommandStreamController =
      StreamController<ButtonsSettingsCommand>.broadcast();

  StreamSink<ButtonsSettingsCommand> get batterySettingsCommandStream => _batterySettingsCommandStreamController.sink;

  StreamController<Parameter> _batterySettingsDataStreamController = StreamController<Parameter>.broadcast(sync: true);

  StreamSink<Parameter> get batterySettingsDataStream => _batterySettingsDataStreamController.sink;

  BatteryTabBloc(this._bluetoothInteractor) {
    _batterySettingsCommandStreamController.stream.listen(_handleCommand);
    _batterySettingsDataStreamController.stream.listen(_handleSettingsData);
  }

  void dispose() {
    _bluetoothInteractor.stopListenSerial();
    _batteryViewModelStreamController.close();
    _batterySettingsCommandStreamController.close();
    _batterySettingsDataStreamController.close();
  }

  void _handleSettingsData(Parameter batteryParameter) {
    print(batteryParameter.name + " = " + batteryParameter.value);
    switch (batteryParameter.name) {
      case "fullVoltage":
        _batterySettings.fullVoltage = int.parse(batteryParameter.value);
        break;
      case "lowVoltage":
        _batterySettings.lowVoltage = int.parse(batteryParameter.value);
        break;
      case "maxPower":
        _batterySettings.maxPower = int.parse(batteryParameter.value);
        break;
      case "driveCurrent":
        _batterySettings.driveCurrent = int.parse(batteryParameter.value);
        break;
      case "regenCurrent":
        _batterySettings.regenCurrent = int.parse(batteryParameter.value);
        break;
      case "powerReductionVoltage":
        _batterySettings.powerReductionVoltage = int.parse(batteryParameter.value);
        break;
    }
  }

  void _handleCommand(ButtonsSettingsCommand event) {
    switch (event) {
      case ButtonsSettingsCommand.READ:
        _batterySettingsRead();
        break;
      case ButtonsSettingsCommand.WRITE:
        _batterySettingsWrite();
        break;
      case ButtonsSettingsCommand.SAVE:
        _batterySettingsSave();
        break;
      case ButtonsSettingsCommand.REFRESH:
        _batterySettingsRefresh();
        break;
    }
  }

  void _packetHandler(Packet packet) {
    //print('BatteryTabBloc   _packetHandler');
    //print(packet.toBytes);
    if (packet.screenNum == SCREEN_NUMBER) {
      if(packet.cmd == readCMD)
      {
        _batterySettings = Mapper.packetToBatterySettings(packet);
        _batterySettingsRefresh();
      }
      if(packet.cmd == responseOK)
      {
        writeResponseState = 1;
        _batterySettingsRefresh();
      }
    }
    if(packet.screenNum == 127) //Save response
        {
      writeResponseState = 2;
      _batterySettingsRefresh();
    }
  }

  void _batterySettingsRead() {
    print("_batterySettingsRead");
    _bluetoothInteractor.sendMessage(Packet(SCREEN_NUMBER, 0, Uint8List(28)));
    _bluetoothInteractor.startListenSerial(_packetHandler);
  }

  void _batterySettingsWrite() {
    Packet packet = Mapper.batterySettingsToPacket(_batterySettings);
    _bluetoothInteractor.sendMessage(packet);
  }

  void _batterySettingsSave() {
    _bluetoothInteractor.save();
  }

  void _batterySettingsRefresh() {
    print("_batterySettingsRefresh");
    _batteryViewModelStreamController.sink.add(_batterySettings);
  }
}
