/*BLOC files represent business logic of app*/
import 'dart:async';
import 'dart:typed_data';
import 'dart:io';
/*package for TIME*/
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';

import 'package:flutter_controller/core/LiveData.dart';
import 'package:flutter_controller/core/Packet.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/MonitorSettings.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';

enum MonitorTabCommand { READ, START_MONITORING, STOP_MONITORING }

///******************************************************************************/
class CounterStorage {
  String formattedDate; //file name
  int fileReady = 0;
  int sampleCounter = 0;
  int logStartTimeMs = 0;
  File myFile;

  String logFilePath;

  Future<String> get _localPath async {
    // storage permission ask
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    }

    final directory = await getExternalStorageDirectory();
    //List<Directory> directory =  await getExternalCacheDirectories();
    return directory.path;
  }

  Future<File> _localFile() async {
    final path = await _localPath;
    return File(path + "/" + formattedDate);
  }

  updateFilePath()
  {
    final directory = getExternalStorageDirectory();
    logFilePath = directory as String;
  }
  openLogFile() async {
    DateTime now = DateTime.now();
    String dateStr = DateFormat('yyyy-MM-dd – kk:mm').format(now) + ".csv";
    if (formattedDate != dateStr) {
      formattedDate = dateStr;
      myFile = await _localFile();
      fileReady = 1;
      sampleCounter = 0;
      logStartTimeMs = now.millisecondsSinceEpoch;
    }
  }

  writeCounter(LiveData liveDataStub, int millisecondsSinceEpoch) async {
    //final file = await _localFile;
    if (fileReady == 1) {
      String sample;
      if (sampleCounter == 0) //MakeHeader
      {
        sample = "TimeMs ;";
        sample += "Ubattery ;"; //1
        sample += "Umotor  ;"; //2
        sample += "Iphase  ;"; //3
        sample += "MotorSpeed ;"; //4
        sample += "Power ;"; //5
        sample += "tController ;"; //6
        sample += "tMotor ;"; //7
        sample += "throttleVolt ;"; //8
        sample += "brakeVolt ;"; //9
        sample += "Flux ;"; //10
        sample += "HallShift ;"; //11
        sample += "hallCnt ;"; //12
        sample += "statusCode ;"; //13
        sample += "errorCode ;"; //14
        sample += " \n";
      } else {
        sample = (millisecondsSinceEpoch.toUnsigned(32) -
                    logStartTimeMs.toUnsigned(32))
                .toString() +
            ";";
        sample += liveDataStub.udc.toString() + ";"; //1
        sample += liveDataStub.uz.toString() + ";"; //2
        sample += liveDataStub.iAmp.toString() + ";"; //3
        sample += liveDataStub.speed.toString() + ";"; //4
        sample += liveDataStub.pel.toString() + ";"; //5
        sample += liveDataStub.tFet.toString() + ";"; //6
        sample += liveDataStub.tMot.toString() + ";"; //7
        sample += liveDataStub.throttleAct.toString() + ";"; //8
        sample += liveDataStub.brake.toString() + ";"; //9
        sample += liveDataStub.activeFlux.toString() + ";"; //10
        sample += liveDataStub.tetaShift.toString() + ";"; //11
        sample += liveDataStub.hallCnt.toString() + ";"; //12
        sample += liveDataStub.statusCode.toString() + ";"; //13
        sample += liveDataStub.errorCode.toString() + ";"; //14
        sample += " \n";
      }
      sampleCounter++;
      // Write the file
      //return file.writeAsString(sample, mode: FileMode.append);
      if (sample.isNotEmpty)
        myFile.writeAsString(sample, mode: FileMode.append);
    }
  }
}

///******************************************************************************/

class MonitorTabBloc {
  static const _SCREEN_NUMBER = 0;

  //static const _MONITORING_INTERVAL = 200;
  LiveData liveDataStub = LiveData.fromBytes(Uint8List(28));
  StreamSubscription _monitoringSubscription;

  MonitorTabSettingsData _settings;
  BluetoothInteractor _interactor;

  StreamController<MonitorTabSettingsData> _monitorSettingsStreamController =
      StreamController<MonitorTabSettingsData>.broadcast();

  Stream<MonitorTabSettingsData> get monitorSettingsStream =>
      _monitorSettingsStreamController.stream;

  StreamController<MonitorTabCommand> _monitorSettingsCommandStreamController =
      StreamController<MonitorTabCommand>.broadcast();

  StreamSink<MonitorTabCommand> get monitorSettingsCommandStream =>
      _monitorSettingsCommandStreamController.sink;

  MonitorTabBloc(this._interactor) {
    _monitorSettingsCommandStreamController.stream.listen(_handleCommand);
  }

  /*looging control */
  CounterStorage dataStore = CounterStorage();

  int logEnabled = 0; //Enable logging
  void logOnOff(bool logControlState) {
    if (logControlState && (logEnabled == 0)) {
      dataStore.openLogFile();
      logEnabled = 1;
    } else {
      logEnabled = 0;
      //dataStore.updateFilePath();
    }
  }

  int getLogState() {
    //return logEnabled;
    var logState =
        Settings.getValue<bool>(MonitorTabSettings.logWriteKey, false);
    logOnOff(logState);
    return (logEnabled = logState ? 1 : 0);
  }

  void _packetHandler(Packet packet) {
    if (packet.screenNum == _SCREEN_NUMBER) {
      LiveData liveDataStub = LiveData.fromBytes(packet.dataBuffer);
      if (logEnabled == 1) {
        DateTime now = DateTime.now();
        dataStore.writeCounter(liveDataStub, now.millisecondsSinceEpoch);
      }
      _settings =
          MonitorTabSettingsData.build(liveDataStub, MonitorTabSettings.load());
      _monitorSettingsStreamController.sink.add(_settings);
    }
  }

  void _stopMonitoring() {
    _interactor.stopListenSerial();
    _monitoringSubscription?.cancel();
    _monitoringSubscription = null;
  }

  void _startMonitoring() async {
    _interactor.startListenSerial(_packetHandler);

    dynamic Function(int) computation = (count) {
      _interactor.sendMessage(Packet(_SCREEN_NUMBER, 0, Uint8List(28)));
    };
    /*periodic callback trig communication event Ask controller state periodically*/
    /*call COMPUTATION function*/
    //Settings.getValue(
    //   MonitorTabSettings.updateRateMs, "1");
    int updateRate = (Settings.getValue<int>(MonitorTabSettings.updateRateMsKey,
        MonitorTabSettings.ratesArray.length - 1));
    Settings.setValue<bool>(MonitorTabSettings.logWriteKey, false);
    if (_monitoringSubscription == null) {
      _monitoringSubscription = Stream.periodic(
              Duration(milliseconds: MonitorTabSettings.ratesArray[updateRate]),
              computation)
          .listen((_) {});
    }
  }

  void _handleCommand(MonitorTabCommand command) {
    switch (command) {
      case MonitorTabCommand.READ:
        _settings = MonitorTabSettingsData.build(
            liveDataStub, MonitorTabSettings.load());
        _monitorSettingsStreamController.sink.add(_settings);
        break;
      case MonitorTabCommand.START_MONITORING:
        _startMonitoring();
        break;
      case MonitorTabCommand.STOP_MONITORING:
        _stopMonitoring();
        break;
    }
  }

  void dispose() {
    _interactor.stopListenSerial();
    _monitorSettingsStreamController.close();
    _monitorSettingsCommandStreamController.close();
  }
}
