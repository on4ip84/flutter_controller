import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_controller/core/Packet.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/Parameter.dart';
import 'package:flutter_controller/model/VirtualButtonsSettings.dart';
import 'package:flutter_controller/util/Mapper.dart';

enum ButtonsSettingsCommand { READ, WRITE, SAVE, REFRESH }

class VirtualButtonsTabBloc {
  static const SCREEN_NUMBER = 11;

  var writeResponseState = 0;

  /*Создаем класс функционала блютуса*/
  BluetoothInteractor _bluetoothInteractor;
  /*Создаем класс настроек для экрана*/
  VirtualButtonsSettings _buttonsSettings;

  /*
    Создаем контроллер потоков для типа класса VirtualButtonsSettings
    Контролллер делаем широковещательным чтоб на него мог модписаться любой виджет?
   */
  StreamController _buttonsViewModelStreamController = StreamController<VirtualButtonsSettings>.broadcast();

  /*
    Создаем get для получения стрима которым управляет созданные streamController
    Контроллер и получатель потока создаются для модели данных ( читать и записывать текущее состояние в модель данных).
   */
  Stream get buttonsViewModelStream => _buttonsViewModelStreamController.stream;

  /*
    Создаем streamController для команд чтения записи обновления
   */
  StreamController<ButtonsSettingsCommand> _buttonsSettingsCommandStreamController =
      StreamController<ButtonsSettingsCommand>.broadcast();
  /*
    Создаем get для этого стрима
   */
  StreamSink<ButtonsSettingsCommand> get buttonsSettingsCommandStream => _buttonsSettingsCommandStreamController.sink;

  /*
    Создаем streamController для параметров
    ОН синхронный???
   */
  StreamController<Parameter> _buttonsSettingsDataStreamController = StreamController<Parameter>.broadcast(sync: true);

  /*
    Создаем get для потлучения потока
   */
  StreamSink<Parameter> get buttonsSettingsDataStream => _buttonsSettingsDataStreamController.sink;

  VirtualButtonsTabBloc(this._bluetoothInteractor) {
    /*
      Данный стрип при поступлении в него данных вызывает обработчик _handleCommand
     */
    _buttonsSettingsCommandStreamController.stream.listen(_handleCommand);
    _buttonsSettingsDataStreamController.stream.listen(_handleSettingsData);
  }

  void dispose() {
    _buttonsViewModelStreamController.close();
    _buttonsSettingsCommandStreamController.close();
    _buttonsSettingsDataStreamController.close();

  }

  void _handleSettingsData(Parameter buttonsParameter) {
    print(buttonsParameter.name + " = " + buttonsParameter.value);
    switch (buttonsParameter.name) {
      case "input1":
        _buttonsSettings.input1 = int.parse(buttonsParameter.value);
        break;
      case "input2":
        _buttonsSettings.input2 = int.parse(buttonsParameter.value);
        break;
      case "input3":
        _buttonsSettings.input3 = int.parse(buttonsParameter.value);
        break;
      case "input4":
        _buttonsSettings.input4 = int.parse(buttonsParameter.value);
        break;
    }
  }

  void _handleCommand(ButtonsSettingsCommand event) {
    switch (event) {
      case ButtonsSettingsCommand.READ:
        _buttonsSettingsRead();
        _buttonsSettingsRefresh();
        break;
      case ButtonsSettingsCommand.WRITE:
        _buttonsSettingsWrite();
        writeResponseState = 0;
        break;
      case ButtonsSettingsCommand.SAVE:
        _buttonsSettingsSave();
        break;
      case ButtonsSettingsCommand.REFRESH:
        _buttonsSettingsRefresh();
        break;
    }
  }

  void _packetHandler(Packet packet) {
    //print('virtualButtonsTabBloc   _packetHandler');
    //print(packet.toBytes);
    if (packet.screenNum == SCREEN_NUMBER) {
      if(packet.cmd == readCMD)
        {
          _buttonsSettings = Mapper.packetToButtonsSettings(packet);
          _buttonsSettingsRefresh();
        }
      if(packet.cmd == responseOK)
        {
          writeResponseState = 1;
          _buttonsSettingsRefresh();
        }
    }
  }

  void _buttonsSettingsRead() {
    print("_virtualButtonsSettingsRead");
    _bluetoothInteractor.sendMessage(Packet(SCREEN_NUMBER, 0, Uint8List(28)));
    _bluetoothInteractor.startListenSerial(_packetHandler);
  }

  void _buttonsSettingsWrite() {
    Packet packet = Mapper.buttonsSettingsToPacket(_buttonsSettings);
    _bluetoothInteractor.sendMessage(packet);
  }

  void _buttonsSettingsSave() {
    _bluetoothInteractor.save();
  }

  void _buttonsSettingsRefresh() {
    print("_virtualButtonsSettingsRefresh");
    _buttonsViewModelStreamController.sink.add(_buttonsSettings);
  }
}
