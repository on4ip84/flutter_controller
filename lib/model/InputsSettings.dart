import 'dart:math';
/*Класс описывает структуру данных Inputs*/

class InputsSettings {
  /* signals from controller ADC*/
  int vdcAdc; //value of ADC on dc bus voltage channel
  int iaAdc; //value of ADC on phase A current channel
  int icAdc; //value of ADC on phase C current channel
  int encoderCounts; //value of incremental encoder counts
  int pwnCounts; //value of pwm pos sensor counts
  double throttleVolts; //calculated voltage from throttle
  double brakeVolts; //calculated voltage form brake
  double v12volts;
  int hallCnt; //counter form hall sensor
  int in1;
  int in2;
  int in3;
  int in4; //physical inputs level
  int in1Can;
  int in2Can;
  int in3Can;
  int in4Can; // virtual inputs level

/*make init for class data*/
  InputsSettings.zero();

  /*сделаем хеш мап с именами и переменными*/
  Map<String, dynamic> toJson() => {
        'vdcAdc': vdcAdc,
        'iaAdc': iaAdc,
        'icAdc': icAdc,
        'encoderCounts': encoderCounts,
        'pwmCounts': pwnCounts,
        'throttleVolts': throttleVolts,
        'brakeVolts': brakeVolts,
        'in1': in1,
        'in2': in2,
        'in3': in3,
        'in4': in4,
        'in1Can': in1Can,
        'in2Can': in2Can,
        'in3Can': in3Can,
        'in4Can': in4Can,
      };
}
