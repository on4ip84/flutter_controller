class RegSettings {
  int currentBandwidth = 0;
  double speedKp = 0;
  double speedKi = 0;
  int speedUpSpeed = 0;
  int speedDownSpeed = 0;
  int wheelDiameter = 1000;
  double mainGear = 1;
  double secondGear =1 ;
  double fieldWeakingKp = 0;
  double fieldWeakingKi = 0;
  int fieldWeakingMaxCurrent = 0;

  RegSettings.zero();

  Map<String, dynamic> toJson() =>
    {
      'currentBandwidth': currentBandwidth,
      'speedKp': speedKp,
      'speedKi': speedKi,
      'fieldWeakingKp': fieldWeakingKp,
      'fieldWeakingKi': fieldWeakingKi,
      'wheelDiam': wheelDiameter,
      'mainGear': mainGear,
      'secondGear': secondGear,
      'speedRampUp': speedUpSpeed,
      'speedRampDown': speedDownSpeed,
      'fieldWeakingMaxCurrent': fieldWeakingMaxCurrent
    };
}