import 'dart:math';
/*Класс описывает даннфе для манипуляций*/
class VirtualButtonsSettings {
  int input1 = 0;
  int input2 = 0;
  int input3 = 0;
  int input4 = 0;
  int input1Real = 0;
  int input2Real = 0;
  int input3Real = 0;
  int input4Real = 0;

  VirtualButtonsSettings.zero();

  VirtualButtonsSettings.random(int range) {
    var random = Random();
    input1 = random.nextInt(range);
    input2 = random.nextInt(range);
    input3 = random.nextInt(range);
    input4 = random.nextInt(range);
  }

  /*сделаем хеш мап с именами и переменными*/
  Map<String, dynamic> toJson() => {
        'input1': input1,
        'input2': input2,
        'input3': input3,
        'input4': input4,
        'input1Real': input1Real,
        'input2Real': input2Real,
        'input3Real': input3Real,
        'input4Real': input4Real,
      };
}
