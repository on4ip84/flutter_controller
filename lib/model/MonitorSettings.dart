import 'package:flutter/material.dart';
import 'package:flutter_controller/core/LiveData.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';

/*!
 *
 * \brief Stucture with Status bit defination
 */ /*
typedef struct
{
uint16_t bit_READY1:1;    */ /*1st ready bit NO error*/ /*
uint16_t bit_READY2:1;    */ /*2nd rady bit NO saturation*/ /*
uint16_t bit_ENABLED:1;    */ /*Modulation enabled Bit*/ /*
uint16_t bit_FAULT:1;      */ /*Global fault bit*/ /*
uint16_t bit_DRIVE_MODE:2;    */ /*Work in motor or gen mode bit*/ /*
uint16_t bit_IDENT_MODE:1;    */ /*Identification but*/ /*
uint16_t bit_SPEED_MODE:1;    */ /*Work in SPeed reg mode*/ /*
uint16_t bit_REVERSE_MODE:1;    */ /*WOrk in reverse*/ /*
uint16_t bit_FW_MODE:1;    */ /*Work in Fieldweaking*/ /*
uint16_t bit_SAT_MODE:1;    */ /*WOkr in saturation*/ /*
uint16_t bit_PROFILE:2;    */ /*Active profile*/ /*
uint16_t bit_HALL_DISABLE:1;  */ /*Hall disabledBit*/ /*
}status_st;
*/ /*!
 *  \brief Structure for Error Bits definitions
 */ /*
typedef struct
{
uint16_t bit_FREE1:1;      */ /*Empty bit*/ /*
uint16_t bit_FREE2:1;      */ /*Empty bit*/ /*
uint16_t bit_AmpCurr_err:1;    */ /*Amplitude current error bit*/ /*
uint16_t bit_Hpower:1;      */ /*Hardware error bit*/ /*
uint16_t bit_Hcurr_err:1;    */ /*Hardware ivercurrent bit*/ /*
uint16_t bit_Hvolt_err:1;    */ /*Hardware overvoltage error bit*/ /*
uint16_t bit_GasFault:1;    */ /*Throttle input error bit*/ /*
uint16_t bit_FREE3:1;      */ /*Empty bit*/ /*
uint16_t bit_OverVolt:1;    */ /*Software overvoltage bit*/ /*
uint16_t bit_OverCurr:1;    */ /*Software overcurrent error error bit*/ /*
uint16_t bit_OverTempBoard:1;  */ /*Software over tempr PowerBoard error bit*/ /*
uint16_t bit_OverTempMotor:1;  */ /*Software over tempr Motor error bit*/ /*
uint16_t bit_OverTempCntr:1;  */ /*Software over tempr COntrol Board error bit*/ /*
uint16_t bit_ISRoverTime:1;    */ /*main ISR over load error bit*/ /*
uint16_t bit_PosSens:1;      */ /*Position sensor error bit*/ /*
}errors_st;*/
class MonitorTabSettings {
  static const centerTopParameter = "centerTopParameter";
  static const leftTopParameter = "leftTopParameter";
  static const leftBottomParameter = "leftBottomParameter";
  static const rightTopParameter = "rightTopParameter";
  static const rightBottomParameter = "rightBottomParameter";
  /*setting for update rate*/
  static const updateRateMsKey = "updateRateMs";
  static const ratesArray = [50,100,250,500,1000];
  static const logWriteKey = "logWriteEnabled";
  /*setting for speed calculation*/
  static const motorPolePairsKey = "motorPolePairs";
  static const wheelDiameterMM = "wheelDiameter";
  static const mainGearKey = "mainGear";
  static const secondGearKey = "secondGear";

  int centerParam;
  int leftTop;
  int leftBottom;
  int rightTop;
  int rightBottom;
  int motorPolePairs;
  int wheelDiam;
  double mainGear;
  double secondGear;
  int updateRateMs;
  String logPath;

  MonitorTabSettings.load() {
    this.centerParam = Settings.getValue<int>(centerTopParameter, 1);
    this.leftTop = Settings.getValue<int>(leftTopParameter, 2);
    this.leftBottom = Settings.getValue<int>(leftBottomParameter, 3);
    this.rightTop = Settings.getValue<int>(rightTopParameter, 4);
    this.rightBottom = Settings.getValue<int>(rightBottomParameter, 5);
    /*Convert string to Number type INT*/
    this.motorPolePairs =
        int.tryParse(Settings.getValue(motorPolePairsKey, "1"));
    this.wheelDiam =
        int.parse(Settings.getValue<String>(wheelDiameterMM, "600"));
    /*Convert string to Number type DOUBLE*/
    this.mainGear = double.parse(Settings.getValue(mainGearKey, "1"));
    this.secondGear = double.parse(Settings.getValue(secondGearKey, "1"));
    this.updateRateMs = (Settings.getValue<int>(updateRateMsKey, 500));
    this.logPath = "";
    //print("centerParam - $centerParam.  leftTop - $leftTop.  leftBottom - $leftBottom.  rightTop - $rightTop.  rightBottom - $rightBottom.  ");
  }
}

class MonitorTabSettingsData {
  ParameterWrapper centerParameter = ParameterWrapper();
  ParameterWrapper leftTop = ParameterWrapper();
  ParameterWrapper leftBottom = ParameterWrapper();
  ParameterWrapper rightTop = ParameterWrapper();
  ParameterWrapper rightBottom = ParameterWrapper();
  String error = "ERROR:NO";
  String mode = "DISABLED";
  String profile = "S1";
  int errorCode;
  final List<String> entriesError = <String>[
    'NO',
    'NO',
    'NO',
    'FET_ERR',
    'OVC',
    'OVV',
    'GAS_ERR',
    'NO',
    'SW_OVV',
    'SW_OVC',
    'HW_OVT',
    'MOT_OVS',
    'CPU_OVT',
    'MOT_OVT',
    'INT_ERR',
    'HALL_ERR'
  ];
  List colorCodesError = [
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey
  ];

  final List<String> entriesState = [];
  List colorCodesState = [
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey,
    Colors.grey
  ];

  MonitorTabSettingsData.build(LiveData liveData, MonitorTabSettings values) {
    //Processing data model for center parameter
    liveData.getParameter(values.centerParam, centerParameter);
    //Processing to other
    liveData.getParameter(values.leftTop, leftTop);
    liveData.getParameter(values.leftBottom, leftBottom);
    liveData.getParameter(values.rightTop, rightTop);
    liveData.getParameter(values.rightBottom, rightBottom);
    /*Error parsing*/
    errorCode = liveData.errorCode;
    //errorCode = 0x1042;     //DEBUG LINE

    if (errorCode > 0) {
      int errorCodeBuffer = errorCode;
      for (int i = 0; i < 16; i++) {
        if ((errorCodeBuffer & 0x01) == 0x1)
          colorCodesError[i] = Colors.redAccent;
        errorCodeBuffer = errorCodeBuffer >> 1;
      }
    } else {
      for (int i = 0; i < 16; i++) {
        colorCodesError[i] = Colors.grey;
      }
    }

    /*Status parsing*/
    entriesState.add("READY");
    entriesState.add("UNLOCK");
    entriesState.add("ACTIVE");
    entriesState.add("FAULT");
    entriesState.add("GENERATOR");
    entriesState.add("IDENT Mode");
    entriesState.add("SPEED Mode");
    entriesState.add("REVERSE Mode");
    entriesState.add("BRAKE Active");
    entriesState.add("FW Mode");
    entriesState.add("SAT Mode");
    entriesState.add("HALL Disabled");
    entriesState.add("Cruise");
    for (int i = 0; i < entriesState.length; i++) {
      if (((liveData.statusCode >> i) & 0x1) == 0x1) {
        colorCodesState[i] = Colors.green;
      } else
        colorCodesState[i] = Colors.grey;
    }
    /*Obrabotka bit*/
    if((colorCodesState[7] == Colors.grey))
      {
        mode = "FWD";
      }else
        {
          mode = "REV";
        }
    /*Obrabotka activnoro profilya*/
    if (((liveData.statusCode >> 13) & 0x1) == 0x1) {
      profile = "S1";
    }
    if (((liveData.statusCode >> 13) & 0x2) == 0x2) {
      profile = "S2";
    }
    if (((liveData.statusCode >> 13) & 0x3) == 0x3) {
      profile = "S3";
    }
  }
}
