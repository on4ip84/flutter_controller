class DriveSettings {
  int throttleUpSpeed = 0;
  int throttleDownSpeed = 0;
  int throttlePhaseCurrentMax = 0;
  int brakeUpSpeed = 0;
  int brakeDownSpeed = 0;
  int brakePhaseCurrentMax = 0;
  int discreetBrakeCurrentUpSpeed = 0;
  int discreetBrakeCurrentDownSpeed = 0;
  int discreetBrakeCurrentMax = 0;
  int autoBrakeMaxCurrent = 0;
  int controlMode = 0;
  int pwmFrequency = 0;
  int fwMode=0;
  int autoBrake=0;
  int decouple=0;
  int dpwm=0;

  DriveSettings.zero();

  Map<String, dynamic> toJson() =>
    {
      'throttleUpSpeed': throttleUpSpeed,
      'throttleDownSpeed': throttleDownSpeed,
      'throttlePhaseCurrentMax': throttlePhaseCurrentMax,
      'brakeUpSpeed': brakeUpSpeed,
      'brakeDownSpeed': brakeDownSpeed,
      'brakePhaseCurrentMax': brakePhaseCurrentMax,
      'discreetBrakeCurrentUpSpeed': discreetBrakeCurrentUpSpeed,
      'discreetBrakeCurrentDownSpeed': discreetBrakeCurrentDownSpeed,
      'discreetBrakeCurrentMax': discreetBrakeCurrentMax,
      'controlMode': controlMode,
      'pwmFrequency': pwmFrequency,
      'autoBrakeCurrent': autoBrakeMaxCurrent,
      'fwMode':fwMode,
      'autoBrake':autoBrake,
      'decouple':decouple,
      'dpwm':dpwm

    };
}