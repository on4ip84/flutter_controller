import 'package:flutter/material.dart';
import 'package:flutter_controller/bloc/MonitorTabBloc.dart';
import 'package:flutter_controller/di/Provider.dart';
import 'package:flutter_controller/interactor/BluetoothInteractor.dart';
import 'package:flutter_controller/model/MonitorSettings.dart';
import 'package:flutter_controller/util/Util.dart';
import 'package:flutter_controller/widget/tab/MonitorTab.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage();

  @override
  _SettingsPage createState() => new _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {
  static const _settings = "settings";
  static const _centerParameter = "centerParameter";
  static const _leftTopParameter = "leftTopParameter";
  static const _leftBottomParameter = "leftBottomParameter";
  static const _rightTopParameter = "rightTopParameter";
  static const _rightBottomParameter = "rightBottomParameter";
  static const _monitorScreenView = "monitorScreenView";
  static const _speedCalcView = "speedCalcView";

  // Здесь задается ключ к поиску названий паметров из списка, параметры выводятся на главный экран
  static const _monitorSettingsParameters = "monitorSettingsParameters";

  Map _localizedStrings;
  Map<int, String> _paramNames;
  BluetoothInteractor _bluetoothInteractor;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bluetoothInteractor = Provider.of(context).bluetoothInteractor;
    _localizedStrings = Provider.of(context).localizedStrings;
    _paramNames = (_localizedStrings[_monitorSettingsParameters] as Map)
        .map((key, value) => MapEntry(int.parse(key), value));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(_localizedStrings[_settings]),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.info_outline),
              onPressed: () {
                showAbout(context);
              },
            ),
            // action button
          ],
        ),
        body: ListView(children: <Widget>[
          ExpandableSettingsTile(
              title: _localizedStrings[_monitorScreenView],
              subtitle: "Central,left,right.....",
              children: <Widget>[
                DropDownSettingsTile<int>(
                  title: _localizedStrings[_centerParameter],
                  settingKey: MonitorTabSettings.centerTopParameter,
                  values: _paramNames,
                  selected: 4,
                ),
                DropDownSettingsTile<int>(
                  title: _localizedStrings[_leftTopParameter],
                  settingKey: MonitorTabSettings.leftTopParameter,
                  values: _paramNames,
                  selected: 1,
                ),
                DropDownSettingsTile<int>(
                  title: _localizedStrings[_leftBottomParameter],
                  settingKey: MonitorTabSettings.leftBottomParameter,
                  values: _paramNames,
                  selected: 2,
                ),
                DropDownSettingsTile<int>(
                  title: _localizedStrings[_rightTopParameter],
                  settingKey: MonitorTabSettings.rightTopParameter,
                  values: _paramNames,
                  selected: 3,
                ),
                DropDownSettingsTile<int>(
                  title: _localizedStrings[_rightBottomParameter],
                  settingKey: MonitorTabSettings.rightBottomParameter,
                  values: _paramNames,
                  selected: 5,
                )
              ]),
          Container(
            height: 1,
          ),
          ExpandableSettingsTile(
            expanded: true,
            title: 'Additional Settings',
            subtitle: 'Speed,Logs,etc...',
            children: [
              SettingsGroup(title: 'System Settings', children: <Widget>[
                SwitchSettingsTile(
                  settingKey: MonitorTabSettings.logWriteKey,
                  title: 'Logging Mode',
                  enabledLabel: 'Enabled',
                  disabledLabel: 'Disabled',
                  leading: Icon(Icons.palette),
                ),
                 DropDownSettingsTile<int>(
                  title: 'Monitor Update rate,ms',
                  settingKey: MonitorTabSettings.updateRateMsKey,
                  values: <int, String>{
                    0: '50',
                    1: '100',
                    2: '250',
                    3: '500',
                    4: '1000',
                  },
                  selected: 3,
                  onChange: (value) {
                    //debugPrint('MonitorTabSettings.updateRateMs: $value')
                  },
                )
              ]),
              SettingsGroup(
                  title: _localizedStrings[_speedCalcView],
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          'Motor Pole Pairs',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 30,
                        ),
                        Text(
                          Settings.getValue(
                              MonitorTabSettings.motorPolePairsKey, "1"),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Wheel Diameter in mm',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 30,
                        ),
                        Text(
                          Settings.getValue(
                              MonitorTabSettings.wheelDiameterMM, "2"),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Main Gear Ratio',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 30,
                        ),
                        Text(
                          Settings.getValue(
                              MonitorTabSettings.mainGearKey, "2"),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Second Gera Ratio',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 30,
                        ),
                        Text(
                          Settings.getValue(
                              MonitorTabSettings.secondGearKey, "2"),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ]),
            ],
          ),
          SettingsGroup(
              //title: _localizedStrings[_speedCalcView],
              title: "Controller Commands",
              children: <Widget>[
                Container(
                  height: 20,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green,
                    foregroundColor: Colors.white,
                    shadowColor: Colors.greenAccent,
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0)),
                    minimumSize: Size(100, 40), //////// HERE
                  ),
                  child: Text(_localizedStrings['restartController']),
                  onPressed: () {
                    _showConfirmationDialog(
                        context, _localizedStrings['askRestartController'], () {
                      _bluetoothInteractor.restartController();
                    });
                  },
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                   backgroundColor: Colors.green,
                    foregroundColor: Colors.white,
                    shadowColor: Colors.greenAccent,
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0)),
                    minimumSize: Size(100, 40), //////// HERE
                  ),
                  child: Text(_localizedStrings['resetToDefaults']),
                  onPressed: () {
                    _showConfirmationDialog(
                        context, _localizedStrings['askResetToDefaults'], () {
                      _bluetoothInteractor.resetController();
                    });
                  },
                ),
                Container(
                  height: 40,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.redAccent,
                    foregroundColor: Colors.white,
                    shadowColor: Colors.redAccent,
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0)),
                    minimumSize: Size(100, 40), //////// HERE
                  ),
                  child: Text(_localizedStrings['bootloaderMode']),
                  onPressed: () {
                    _showConfirmationDialog(
                        context, _localizedStrings['askBootloaderMode'], () {
                      _bluetoothInteractor.turnToBootloaderMode();
                    });
                  },
                )
              ])
        ]));
  }

  Future<void> _showConfirmationDialog(
      BuildContext context, String confirmationText, Function onApprove) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(_localizedStrings['confirmation']),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(confirmationText),
              ],
            ),
          ),
          actions: <Widget>[
            ElevatedButton(
              child: const Text('OK'),
              onPressed: () {
                onApprove();
                Navigator.of(context).pop();
              },
            ),
            Container(
              width: 8,
            ),
            ElevatedButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
